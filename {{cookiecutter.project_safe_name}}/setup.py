import setuptools as sut

with open("VERSION") as fh:
    VERSION = fh.read()

with open("README.md", "r") as fh:
    LONG_DESCRIPTION = fh.read()

sut.setup(
    name="{{cookiecutter.project_safe_name}}",
    version=VERSION,
    author="{{cookiecutter.full_name}}",
    author_email="{{cookiecutter.email}}",
    description="{{cookiecutter.short_description}}",
    long_description_content_type="text/markdown",
    long_description=LONG_DESCRIPTION,
    url="{{cookiecutter.gitlab_url}}",
    packages=sut.find_packages(where="src"),
    package_dir={"": "src"},
    license="Proprietary",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Other/Proprietary License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    install_requires=["typer"],
    tests_require=["pytest"],
    include_package_data=True,
    test_suite="tests",
    entry_points={"console_scripts": ["{{cookiecutter.project_safe_name}}={{cookiecutter.project_package_name}}.cli:main"]},
)
