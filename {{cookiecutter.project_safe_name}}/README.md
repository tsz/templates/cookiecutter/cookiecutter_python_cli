# {{cookiecutter.project_name}}

## Building and installing

To install this package locally for development it is assumed that you have **pyenv** and **poetry** installed. Assuming that is the case follow these steps:

1. ```git clone {{cookiecutter.gitlab_git}}```
1. ```cd {{cookiecutter.project_safe_name}}```
1. Check that you have a python version compatible with python {{cookiecutter.python_version}}. If not, install with pyenv by doing
    * ```pyenv install {{cookiecutter.python_version}}```
1. Set up the virtual environment:
    * ```pyenv virtualenv {{cookiecutter.python_version}} {{cookiecutter.project_venv_name}}```
1. ```pyenv local {{cookiecutter.project_venv_name}}```
1. ```poetry install```

If everything has worked you should be able to run:

```invoke check```

## Testing

Include some notes here about testing the project.

## Running the project

Include some notes here about running the project.
