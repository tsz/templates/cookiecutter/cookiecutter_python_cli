import typer


def main():
    """Console script for {{cookiecutter.project_safe_name}}."""
    typer.echo(
        "Replace this message by putting your code into "
        + "{{cookiecutter.project_package_name}}.cli.main in the src directory"
    )
    typer.echo("See typer documentation at https://typer.tiangolo.com/tutorial/first-steps/")


if __name__ == "__main__":
    typer.run(main)
