import os
import git
import requests as req


PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)
GITLAB_TOKEN = os.getenv("TSZ_GITLAB_TOKEN")


def get_gitlab_namespace_id(headers):
    group = '{{cookiecutter.gitlab_group_path}}'

    res = req.get('https://gitlab.com/api/v4/namespaces?per_page=100', 
                  headers=headers)
    ns = res.json()
    try:
        ns_id = list(filter(lambda x: x['full_path'] == group, ns))[0]['id']
    except IndexError:
        raise ValueError('group not found on gitlab')

    return ns_id

def create_gitlab_repo(namespace_id, headers):
    data = {
        'name': '{{cookiecutter.project_name}}',
        'path': '{{cookiecutter.project_safe_name}}',
        'namespace_id': namespace_id,
        'description': '{{cookiecutter.short_description}}',
        'visibility': 'private',
    }
    print(data)
    res = req.post('https://gitlab.com/api/v4/projects',
                   headers=headers, data=data)
    if res.status_code in [200, 201, 202, 204]:
        print('Project repository successfully created on GitLab')
    else:
        print(res)
        print('Unable to create project repository on GitLab')

def set_up_venv():
    cmd = '''
        pyenv virtualenv {{cookiecutter.python_version}} {{cookiecutter.project_venv_name}} &&
        pyenv local {{cookiecutter.project_venv_name}}
        poetry install
    '''
    
    os.system(cmd)


def git_init_commit(r):
    r.index.add(['*', '.gitignore', '.sample-gitlab-ci.yml', '.pre-commit-config.yaml'])
    r.index.commit('initial commit')
    return r


def set_up_pre_commit():
    cmd = "pre-commit install"
    os.system(cmd)


def main():
    r = git.Repo.init(PROJECT_DIRECTORY)
    set_up_venv()
    #set_up_pre_commit()
    
    r = git_init_commit(r)
    
    if {{cookiecutter.create_gitlab_project}}:
        origin = r.create_remote(name='origin',
                                 url='{{cookiecutter.gitlab_git}}')
        headers = {'PRIVATE-TOKEN': GITLAB_TOKEN}
        ns_id = get_gitlab_namespace_id(headers)
        create_gitlab_repo(ns_id, headers)
        
        r.git.push('--set-upstream', 'origin', 'master')


if __name__ == "__main__":
    main()